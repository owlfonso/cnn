# import the necessary packages
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import pickle
from PIL import Image 

# load the trained convolutional neural network
print("[INFO] loading network...")
model = load_model('cnn/mycnn.model')
my = pickle.loads(open('cnn/my.pickle', "rb").read())

# load test image
imagepath = 'cnn/Test_Dataset/pizza-napoletana.jpg'
image = Image.open(imagepath)

# pre-process the image for classification
image = image.resize((200,200))
image = img_to_array(image)
image = image / 255.0
image = np.expand_dims(image, axis=0)



# classify the input image then find the indexes of the class
# labels with the *largest* probability
print("[INFO] classifying image...")
proba = model.predict(image)[0]
idxs = np.argsort(proba)[::-1][:1]
for (i, j) in enumerate(idxs):
    # build the label and draw the label on the image
    label = "{}: {:.2f}%".format(my.classes_[j], proba[j] * 100)
         
print(imagepath)       
print("Classified as-> " + label)

# show the probabilities for each of the individual labels
for (label, p) in zip(my.classes_, proba):
    print("{}: {:.2f}%".format(label, p * 100))


